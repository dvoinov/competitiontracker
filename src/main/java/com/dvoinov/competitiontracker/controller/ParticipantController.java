package com.dvoinov.competitiontracker.controller;

import com.dvoinov.competitiontracker.domain.Participant;
import com.dvoinov.competitiontracker.repos.ParticipantRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


@Controller
public class ParticipantController {

    @Autowired
    private ParticipantRepo participantRepo;

    @GetMapping("/addparticipant")
    public String addparticipant(Map<String, Object> model) {

        Iterable<Participant> participants = participantRepo.findAll();

        model.put("participants", participants);

        return "addparticipant";
    }

    @PostMapping("/addparticipant")
    public String add(@RequestParam String surname, @RequestParam String name, Map<String, Object> model) {
        Participant message = new Participant(surname, name);

        participantRepo.save(message);

        Iterable<Participant> participants = participantRepo.findAll();

        model.put("participants", participants);

        return "addparticipant";
    }

    @PostMapping("filter")
    public String filter(@RequestParam String filter, Map<String, Object> model) {

        Iterable<Participant> participants;

        if (filter !=null && !filter.isEmpty()) {
            participants = participantRepo.findBySurname(filter);
        } else {
            participants = participantRepo.findAll();
        }

        model.put("participants", participants);

        return "addparticipant";
    }
}
