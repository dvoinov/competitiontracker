package com.dvoinov.competitiontracker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@Controller
public class MainController {
    @GetMapping("/")
    public String greeting() {

        return "greeting";
    }

    @GetMapping("/main")
    public String main() {
        return "main";
    }

    @PostMapping("/main")
    public String main(Map<String, Object> model) {
        return "main";
    }
}
