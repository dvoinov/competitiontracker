package com.dvoinov.competitiontracker.repos;

import com.dvoinov.competitiontracker.domain.Participant;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ParticipantRepo extends CrudRepository<Participant, Long> {

    List<Participant> findBySurname(String surname);
}
